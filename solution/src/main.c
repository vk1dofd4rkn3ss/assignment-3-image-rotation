#include "bmp.h"
#include "header.h"
#include "image.h"
#include "rotate.h"

#define NUM_OF_ARGS 3
#define ERROR_CODE 1
#define SUCCESS_CODE 0

int main(int argc, char** argv) {
	if (argc != NUM_OF_ARGS) {
		printf("Неправильный ввод. Необходимое количество аргументов - 2!\n");
		return ERROR_CODE;
	}

    FILE *input = fopen(argv[1], "rb");
    FILE *output = fopen(argv[2], "wb");

    if (input == NULL) {
        printf("Файл не может быть открыт!\n");
        return ERROR_CODE;
    }

    struct image orig = {0};

    if (from_bmp(input, &orig) != READ_OK) {
        printf("Ошибка прочтения изображения!\n");
        free_image(&orig);
        fclose(input);
        fclose(output);
        return ERROR_CODE;
    }

    struct image rotated = rotate(&orig);

    if (to_bmp(output, &rotated) != WRITE_OK) {
        printf("Ошибка! Не получается сохранить изображение!\n");
        free_image(&orig);
        free_image(&rotated);
        fclose(input);
        fclose(output);
        return ERROR_CODE;
    }

    printf("Изображение сохранено.\n");
    free_image(&orig);
    free_image(&rotated);
    fclose(input);
    fclose(output);
	return SUCCESS_CODE;
}
