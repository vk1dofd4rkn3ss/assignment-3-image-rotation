#include "../include/rotate.h"

struct image rotate(struct image *img) {
    struct image rotated_img = create_image(img->height, img->width);
    for (uint64_t i = 0; i < img->width; i++) {
        for (uint64_t j = 0; j < img->height; j++) {
            set_pixel(&rotated_img, get_pixel(img, i, j), img->height - j - 1, i);
        }
    }
    return rotated_img;
}
